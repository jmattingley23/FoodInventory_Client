package xyz.jmatt.foodinventoryclient.holders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import xyz.jmatt.foodinventoryclient.R;
import xyz.jmatt.foodinventoryclient.activities.ItemActivity;
import xyz.jmatt.foodinventoryclient.models.ClientSingletonModel;
import xyz.jmatt.models.ItemModel;

public class SearchItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private ImageView image;
    private TextView title;
    private TextView barcode;
    private TextView quantity;

    private ItemModel model;
    private Context context;

    public SearchItemHolder(LayoutInflater inflater, ViewGroup parent) {
        super(inflater.inflate(R.layout.search_item, parent, false));
        image = (ImageView)itemView.findViewById(R.id.search_item_image);
        title = (TextView)itemView.findViewById(R.id.search_item_title);
        barcode = (TextView)itemView.findViewById(R.id.search_item_barcode);
        quantity = (TextView)itemView.findViewById(R.id.search_item_quantity);
        itemView.setOnClickListener(this);
    }

    public void bind(ItemModel model, Context context) {
        this.model = model;
        this.context = context;
        title.setText(model.getTitle());
        if(model.getImage() == null) {
            image.setImageResource(R.drawable.blank);
        } else {
            ClientSingletonModel.getInstance().getImageLoader().DisplayImage(model.getImage(), image);
        }
        barcode.setText(model.getFormattedBarcode());
        quantity.setText(String.valueOf(model.getQuantity()));
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(context, ItemActivity.class);
        intent.putExtra(context.getString(R.string.tag_barcode), model.getBarcode());
        context.startActivity(intent);
    }
}
