package xyz.jmatt.foodinventoryclient.holders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import xyz.jmatt.foodinventoryclient.R;
import xyz.jmatt.foodinventoryclient.activities.ItemActivity;
import xyz.jmatt.foodinventoryclient.models.ClientSingletonModel;
import xyz.jmatt.models.ItemModel;

public class InventoryItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView lastAdded;
    private TextView title;
    private TextView quantity;
    private ImageView image;
    private TextView expiration;

    private Context context;
    private ItemModel model;

    public InventoryItemHolder(LayoutInflater inflater, ViewGroup parent) {
        super(inflater.inflate(R.layout.inventory_item, parent, false));
        lastAdded = (TextView)itemView.findViewById(R.id.inventory_item_last_added);
        title = (TextView)itemView.findViewById(R.id.inventory_item_title);
        quantity = (TextView)itemView.findViewById(R.id.inventory_item_quantity);
        image = (ImageView)itemView.findViewById(R.id.inventory_item_image);
        expiration = (TextView)itemView.findViewById(R.id.inventory_item_expiration);
        itemView.setOnClickListener(this);
    }

    public void bind(ItemModel model, Context context) {
        this.context = context;
        this.model = model;
        Date lastAddedDate = new Date(model.getLastAdded());
        SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a EEE, MMM d, yyyy", Locale.US);
        lastAdded.setText(dateFormat.format(lastAddedDate));
        title.setText(model.getTitle());
        quantity.setText(String.valueOf(model.getQuantity()));
        if(model.getImage() == null) {
            image.setImageResource(R.drawable.blank);
        } else {
            ClientSingletonModel.getInstance().getImageLoader().DisplayImage(model.getImage(), image);
        }
        if(model.getExpiration() == 0) {
            expiration.setText(R.string.none);
        } else {
            Date expirationDate = new Date(model.getExpiration());
            SimpleDateFormat expirationFormat = new SimpleDateFormat("MMM, d, yyyy", Locale.US);
            expiration.setText(expirationFormat.format(expirationDate));
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(context, ItemActivity.class);
        intent.putExtra(context.getString(R.string.tag_barcode), model.getBarcode());
        context.startActivity(intent);
    }
}
