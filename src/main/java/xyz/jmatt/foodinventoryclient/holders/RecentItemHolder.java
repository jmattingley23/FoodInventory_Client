package xyz.jmatt.foodinventoryclient.holders;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import xyz.jmatt.foodinventoryclient.R;
import xyz.jmatt.foodinventoryclient.activities.ItemActivity;
import xyz.jmatt.foodinventoryclient.models.ClientSingletonModel;
import xyz.jmatt.models.ItemModel;

/**
 * Puts information from an ItemModel object into a RecentItem view
 */
public class RecentItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView title;
    private TextView barcode;
    private TextView lastAdded;
    private ImageView image;
    private TextView quantity;

    private ItemModel item;
    private Context context;

    public RecentItemHolder(LayoutInflater inflater, ViewGroup parent) {
        super(inflater.inflate(R.layout.recent_item, parent, false));

        //gather necessary views
        title = (TextView)itemView.findViewById(R.id.recent_item_title);
        barcode = (TextView)itemView.findViewById(R.id.recent_item_barcode);
        lastAdded = (TextView)itemView.findViewById(R.id.recent_item_last_added);
        image = (ImageView)itemView.findViewById(R.id.recent_item_image);
        quantity = (TextView)itemView.findViewById(R.id.recent_item_quantity);
        Button editBtn = (Button)itemView.findViewById(R.id.recent_item_edit_btn);
        editBtn.setOnClickListener(this);
    }

    /**
     * fills the RecentItem view with data from the given ItemModel
     * @param item the ItemModel to gather data from
     */
    public void bind(ItemModel item, Context context) {
        this.item = item;
        this.context = context;
        //populate views with data
        title.setText(item.getTitle());
        barcode.setText(item.getFormattedBarcode());
        Date lastAddedDate = new Date(item.getLastAdded());
        SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a EEE, MMM d, yyyy", Locale.US);
        lastAdded.setText(dateFormat.format(lastAddedDate));
        if(item.getImage() == null) {
            image.setImageResource(R.drawable.blank);
        } else {
            ClientSingletonModel.getInstance().getImageLoader().DisplayImage(item.getImage(), image);
        }
        quantity.setText(String.valueOf(item.getQuantity()));
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(context, ItemActivity.class);
        intent.putExtra(context.getString(R.string.tag_barcode), item.getBarcode());
        context.startActivity(intent);
    }
}
