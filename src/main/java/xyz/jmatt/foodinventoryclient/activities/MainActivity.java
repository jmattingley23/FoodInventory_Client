package xyz.jmatt.foodinventoryclient.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import xyz.jmatt.foodinventoryclient.R;
import xyz.jmatt.foodinventoryclient.fragments.ActionBarFragment;
import xyz.jmatt.foodinventoryclient.fragments.HomeFragment;
import xyz.jmatt.foodinventoryclient.models.ClientSingletonModel;

public class MainActivity extends BaseNavActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setNavSelection(BaseNavActivity.HOME_POSITION);

        ClientSingletonModel.getProxy().connect("192.168.2.28", "70");
        ClientSingletonModel.getInstance().logCurrentToken();

        ClientSingletonModel.getInstance().initImageLoader(this);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        ActionBarFragment actionBarFragment = new ActionBarFragment();
        Bundle args = new Bundle();
        args.putBoolean(getString(R.string.tag_flatten), true);
        args.putString(getString(R.string.tag_title), getString(R.string.food_inventory));
        actionBarFragment.setArguments(args);
        fragmentTransaction.replace(R.id.content_frame, actionBarFragment);
        fragmentTransaction.replace(R.id.fragment_frame, new HomeFragment());
        fragmentTransaction.commit();
    }
}
