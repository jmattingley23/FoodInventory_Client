package xyz.jmatt.foodinventoryclient.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;

import xyz.jmatt.foodinventoryclient.R;
import xyz.jmatt.foodinventoryclient.fragments.ActionBarFragment;
import xyz.jmatt.foodinventoryclient.fragments.InventoryFragment;

public class InventoryActivity extends BaseNavActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setNavSelection(BaseNavActivity.INVENTORY_POSITION);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        ActionBarFragment actionBarFragment = new ActionBarFragment();
        Bundle args = new Bundle();
        args.putString(getString(R.string.tag_title), getString(R.string.inventory));
        actionBarFragment.setArguments(args);
        fragmentTransaction.replace(R.id.content_frame, actionBarFragment);
        fragmentTransaction.replace(R.id.fragment_frame, new InventoryFragment());
        fragmentTransaction.commit();
    }
}
