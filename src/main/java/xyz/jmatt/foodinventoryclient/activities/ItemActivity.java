package xyz.jmatt.foodinventoryclient.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import xyz.jmatt.foodinventoryclient.R;
import xyz.jmatt.foodinventoryclient.fragments.ActionBarFragment;
import xyz.jmatt.foodinventoryclient.fragments.ItemFragment;

public class ItemActivity extends BaseNavActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setNavSelection(BaseNavActivity.INVENTORY_POSITION);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        ActionBarFragment actionBarFragment = new ActionBarFragment();
        Bundle args = new Bundle();
        args.putString(getString(R.string.tag_title), "Item Details");
        actionBarFragment.setArguments(args);
        fragmentTransaction.replace(R.id.content_frame, actionBarFragment);

        ItemFragment itemFragment = new ItemFragment();
        Intent intent = getIntent();
        String barcode = intent.getStringExtra(getString(R.string.tag_barcode));
        Bundle itemArgs = new Bundle();
        itemArgs.putString(getString(R.string.tag_barcode), barcode);
        itemFragment.setArguments(itemArgs);
        fragmentTransaction.replace(R.id.fragment_frame, itemFragment, getString(R.string.tag_item_fragment));

        fragmentTransaction.commit();
    }
}
