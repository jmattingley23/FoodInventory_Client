package xyz.jmatt.foodinventoryclient.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.widget.ListView;
import android.widget.Toast;

import xyz.jmatt.foodinventoryclient.R;
import xyz.jmatt.foodinventoryclient.adapters.NavItemAdapter;
import xyz.jmatt.foodinventoryclient.models.NavItem;

public class BaseNavActivity extends AppCompatActivity {
    protected DrawerLayout navDrawer;
    private ListView drawerList;

    private ProgressDialog loadingModal;

    public static final int HOME_POSITION = 0;
    public static final int SEARCH_POSITION = 1;
    public static final int INVENTORY_POSITION = 2;
    public static final int SETTINGS_POSITION = 3;

    private BroadcastReceiver fcmReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.navigation_drawer);
        navDrawer = (DrawerLayout)findViewById(R.id.nav_drawer_layout);
        NavItem[] navItems = {
                new NavItem(getString(R.string.home), R.drawable.ic_home_24dp, MainActivity.class),
                new NavItem(getString(R.string.search), R.drawable.ic_search_24dp, SearchActivity.class),
                new NavItem(getString(R.string.inventory), R.drawable.ic_grid_24dp, InventoryActivity.class),
                new NavItem(getString(R.string.settings), R.drawable.ic_settings_24dp, SettingsActivity.class)};
        NavItemAdapter adapter = new NavItemAdapter(this, R.layout.navbar_item, navItems);
        drawerList = (ListView)findViewById(R.id.nav_drawer_list);
        drawerList.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        fcmReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String message = intent.getStringExtra(getString(R.string.fcm_message));
                toast(message);
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(fcmReceiver, new IntentFilter(getString(R.string.fcm_message)));
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(fcmReceiver);
    }

    public void setNavSelection(final int position) {
        drawerList.clearFocus();
        drawerList.post(new Runnable() {
            @Override
            public void run() {
                drawerList.setItemChecked(position, true);
            }
        });
    }

    public void openDrawer() {
        navDrawer.openDrawer(Gravity.START);
    }

    /**
     * Displays the given text on screen in a short Toast
     * @param text the text to display
     */
    public void toast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    public void startLoadingModal(String message) {
        if(loadingModal == null) {
            loadingModal = new ProgressDialog(this);
            loadingModal.setMessage(message);
            loadingModal.setCancelable(false);
            loadingModal.show();
        }
    }

    public void dismissLoadingModal() {
        if(loadingModal != null && loadingModal.isShowing()) {
            loadingModal.dismiss();
            loadingModal = null;
        }
    }
}
