package xyz.jmatt.foodinventoryclient.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;

import xyz.jmatt.foodinventoryclient.R;
import xyz.jmatt.foodinventoryclient.fragments.ActionBarFragment;
import xyz.jmatt.foodinventoryclient.fragments.SearchFragment;

public class SearchActivity extends BaseNavActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setNavSelection(BaseNavActivity.SEARCH_POSITION);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        ActionBarFragment actionBarFragment = new ActionBarFragment();
        Bundle args = new Bundle();
        args.putString(getString(R.string.tag_title), getString(R.string.search));
        args.putBoolean(getString(R.string.tag_flatten), true);
        args.putBoolean(getString(R.string.tag_hide_search), true);
        actionBarFragment.setArguments(args);
        fragmentTransaction.replace(R.id.content_frame, actionBarFragment);
        fragmentTransaction.replace(R.id.fragment_frame, new SearchFragment());
        fragmentTransaction.commit();
    }
}
