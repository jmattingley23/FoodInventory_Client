package xyz.jmatt.foodinventoryclient.asynctasks;

import android.os.AsyncTask;

import xyz.jmatt.foodinventoryclient.activities.BaseNavActivity;
import xyz.jmatt.foodinventoryclient.fragments.HomeFragment;
import xyz.jmatt.foodinventoryclient.models.ClientSingletonModel;
import xyz.jmatt.results.SimpleResult;

public class ProcessItemTask extends AsyncTask<String, Void, SimpleResult> {
    private BaseNavActivity activity;

    public ProcessItemTask(BaseNavActivity activity) {
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        activity.startLoadingModal("Submitting barcode...");
    }

    @Override
    protected SimpleResult doInBackground(String... barcodes) {
        return ClientSingletonModel.getProxy().processItem(barcodes[0]);
    }

    @Override
    protected void onPostExecute(SimpleResult simpleResult) {
        super.onPostExecute(simpleResult);
        activity.dismissLoadingModal();
        if(simpleResult.isError()) {
            activity.toast(simpleResult.getMessage());
        }
    }
}
