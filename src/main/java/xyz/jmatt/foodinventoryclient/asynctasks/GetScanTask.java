package xyz.jmatt.foodinventoryclient.asynctasks;

import android.os.AsyncTask;

import xyz.jmatt.foodinventoryclient.activities.BaseNavActivity;
import xyz.jmatt.foodinventoryclient.fragments.HomeFragment;
import xyz.jmatt.foodinventoryclient.models.ClientSingletonModel;
import xyz.jmatt.results.SimpleResult;

public class GetScanTask extends AsyncTask<Void, Void, SimpleResult> {
    private BaseNavActivity activity;
    private HomeFragment fragment;

    public GetScanTask(BaseNavActivity activity, HomeFragment fragment) {
        this.activity = activity;
        this.fragment = fragment;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        activity.startLoadingModal("Getting scanner state from server...");
    }

    @Override
    protected SimpleResult doInBackground(Void... params) {
        return ClientSingletonModel.getProxy().getScanState();
    }

    @Override
    protected void onPostExecute(SimpleResult simpleResult) {
        super.onPostExecute(simpleResult);
        activity.dismissLoadingModal();
        if(!simpleResult.isError()) {
            fragment.updateScanButtonStyling(simpleResult.getMessage());
        } else {
            activity.toast(simpleResult.getMessage());
        }
    }
}
