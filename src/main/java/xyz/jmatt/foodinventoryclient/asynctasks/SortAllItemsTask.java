package xyz.jmatt.foodinventoryclient.asynctasks;

import android.os.AsyncTask;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import xyz.jmatt.foodinventoryclient.fragments.InventoryFragment;
import xyz.jmatt.foodinventoryclient.models.ClientSingletonModel;
import xyz.jmatt.models.ItemModel;

public class SortAllItemsTask extends AsyncTask<Integer, Void, List<ItemModel>> {
    private static final int RECENTLY_UPDATED_POS = 0;
    private static final int ALPHABETICAL_POS = 1;
    private static final int QUANTITY_POS = 2;
    private static final int EXPIRATION_POS = 3;
    private static final int DESCENDING_POS = 0;
    private static final int ASCENDING_POS = 1;

    private List<ItemModel> items;
    private InventoryFragment fragment;

    public SortAllItemsTask(List<ItemModel> items, InventoryFragment fragment) {
        this.items = items;
        this.fragment = fragment;
    }

    @Override
    protected List<ItemModel> doInBackground(Integer... params) {
        int typeSpinner = params[0];
        int orderSpinner = params[1];

        if(typeSpinner == RECENTLY_UPDATED_POS && orderSpinner == ASCENDING_POS) {
            sortRecentAsc();
        } else if(typeSpinner == RECENTLY_UPDATED_POS && orderSpinner == DESCENDING_POS) {
            sortRecentDesc();
        } else if(typeSpinner == ALPHABETICAL_POS && orderSpinner == ASCENDING_POS) {
            sortAlphaAsc();
        } else if(typeSpinner == ALPHABETICAL_POS && orderSpinner == DESCENDING_POS) {
            sortAlphaDesc();
        } else if(typeSpinner == QUANTITY_POS && orderSpinner == ASCENDING_POS) {
            sortQuantityAsc();
        } else if(typeSpinner == QUANTITY_POS && orderSpinner == DESCENDING_POS) {
            sortQuantityDesc();
        } else if(typeSpinner == EXPIRATION_POS && orderSpinner == ASCENDING_POS) {
            sortExpirationAsc();
        } else if(typeSpinner == EXPIRATION_POS && orderSpinner == DESCENDING_POS) {
            sortExpirationDesc();
        }

        return items;
    }

    @Override
    protected void onPostExecute(List<ItemModel> itemModels) {
        super.onPostExecute(itemModels);
        ClientSingletonModel.getInstance().setAllItems(itemModels);
        fragment.updateInventory();
    }

    private void sortRecentAsc() {
        Collections.sort(items, new Comparator<ItemModel>() {
            @Override
            public int compare(ItemModel o1, ItemModel o2) {
                return (int)(o1.getLastAdded() - o2.getLastAdded());
            }
        });
    }

    private void sortRecentDesc() {
        Collections.sort(items, new Comparator<ItemModel>() {
            @Override
            public int compare(ItemModel o1, ItemModel o2) {
                return (int)(o2.getLastAdded() - o1.getLastAdded());
            }
        });
    }
    private void sortAlphaAsc() {
        Collections.sort(items, new Comparator<ItemModel>() {
            @Override
            public int compare(ItemModel o1, ItemModel o2) {
                return o1.getTitle().toLowerCase().compareTo(o2.getTitle().toLowerCase());
            }
        });
    }
    private void sortAlphaDesc() {
        Collections.sort(items, new Comparator<ItemModel>() {
            @Override
            public int compare(ItemModel o1, ItemModel o2) {
                return o2.getTitle().toLowerCase().compareTo(o1.getTitle().toLowerCase());
            }
        });
    }
    private void sortQuantityAsc() {
        Collections.sort(items, new Comparator<ItemModel>() {
            @Override
            public int compare(ItemModel o1, ItemModel o2) {
                return o1.getQuantity() - o2.getQuantity();
            }
        });
    }
    private void sortQuantityDesc() {
        Collections.sort(items, new Comparator<ItemModel>() {
            @Override
            public int compare(ItemModel o1, ItemModel o2) {
                return o2.getQuantity() - o1.getQuantity();
            }
        });
    }
    private void sortExpirationAsc() {
        Collections.sort(items, new Comparator<ItemModel>() {
            @Override
            public int compare(ItemModel o1, ItemModel o2) {
                return (int)(o1.getExpiration() - o2.getExpiration());
            }
        });
    }
    private void sortExpirationDesc() {
        Collections.sort(items, new Comparator<ItemModel>() {
            @Override
            public int compare(ItemModel o1, ItemModel o2) {
                return (int)(o2.getExpiration() - o1.getExpiration());
            }
        });
    }
}
