package xyz.jmatt.foodinventoryclient.asynctasks;

import android.os.AsyncTask;

import xyz.jmatt.foodinventoryclient.AbstractInventory;
import xyz.jmatt.foodinventoryclient.activities.BaseNavActivity;
import xyz.jmatt.foodinventoryclient.models.ClientSingletonModel;
import xyz.jmatt.models.ItemModel;

public class GetAllItemsTask extends AsyncTask<Void, Void, ItemModel> {
    private BaseNavActivity activity;
    private AbstractInventory inventory;

    public GetAllItemsTask(BaseNavActivity activity, AbstractInventory inventory) {
        this.activity = activity;
        this.inventory = inventory;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        activity.startLoadingModal("Fetching all items from server...");
    }

    @Override
    protected ItemModel doInBackground(Void... params) {
        return ClientSingletonModel.getProxy().getAllItems();
    }

    @Override
    protected void onPostExecute(ItemModel itemModel) {
        super.onPostExecute(itemModel);
        activity.dismissLoadingModal();
        if(itemModel.isError()) {
            activity.toast(itemModel.getMessage());
        } else {
            ClientSingletonModel.getInstance().setAllItems(itemModel.getItems());
            inventory.markServerReady();
        }
    }
}
