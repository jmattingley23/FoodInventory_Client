package xyz.jmatt.foodinventoryclient.asynctasks;

import android.os.AsyncTask;

import xyz.jmatt.foodinventoryclient.models.ClientSingletonModel;
import xyz.jmatt.results.SimpleResult;

public class UpdateTokenTask extends AsyncTask<String, Void, SimpleResult> {
    @Override
    protected SimpleResult doInBackground(String... params) {
        return ClientSingletonModel.getProxy().setClientToken(params[0]);
    }
}
