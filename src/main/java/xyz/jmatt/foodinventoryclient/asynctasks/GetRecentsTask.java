package xyz.jmatt.foodinventoryclient.asynctasks;

import android.os.AsyncTask;

import xyz.jmatt.foodinventoryclient.activities.BaseNavActivity;
import xyz.jmatt.foodinventoryclient.fragments.HomeFragment;
import xyz.jmatt.foodinventoryclient.models.ClientSingletonModel;
import xyz.jmatt.models.ItemModel;

public class GetRecentsTask extends AsyncTask<Void, Void, ItemModel> {
    private BaseNavActivity activity;
    private HomeFragment fragment;

    public GetRecentsTask(BaseNavActivity activity, HomeFragment fragment) {
        this.activity = activity;
        this.fragment = fragment;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        activity.startLoadingModal("Updating recent items list...");
    }

    @Override
    protected ItemModel doInBackground(Void... params) {
        return ClientSingletonModel.getProxy().getRecentItems();
    }

    @Override
    protected void onPostExecute(ItemModel itemModel) {
        super.onPostExecute(itemModel);
        activity.dismissLoadingModal();
        if(itemModel.isError()) {
            activity.toast(itemModel.getMessage());
        } else {
            ClientSingletonModel.getInstance().setRecentItems(itemModel.getItems());
            fragment.refreshRecentItems();
        }
    }
}
