package xyz.jmatt.foodinventoryclient.asynctasks;

import android.os.AsyncTask;

import xyz.jmatt.foodinventoryclient.activities.BaseNavActivity;
import xyz.jmatt.foodinventoryclient.models.ClientSingletonModel;
import xyz.jmatt.models.ItemModel;
import xyz.jmatt.results.SimpleResult;

public class UpdateItemTask extends AsyncTask<ItemModel, Void, SimpleResult> {
    private BaseNavActivity activity;

    public UpdateItemTask(BaseNavActivity activity) {
            this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        activity.startLoadingModal("Updating item...");
    }

    @Override
    protected SimpleResult doInBackground(ItemModel... params) {
        return ClientSingletonModel.getProxy().updateItemModel(params[0]);
    }

    @Override
    protected void onPostExecute(SimpleResult simpleResult) {
        super.onPostExecute(simpleResult);
        activity.dismissLoadingModal();
        if(simpleResult.isError()) {
            activity.toast(simpleResult.getMessage());
        } else {
            activity.toast("Item updated successfully");
        }
    }
}
