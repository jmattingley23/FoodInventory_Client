package xyz.jmatt.foodinventoryclient.asynctasks;

import android.os.AsyncTask;

import xyz.jmatt.foodinventoryclient.activities.BaseNavActivity;
import xyz.jmatt.foodinventoryclient.fragments.HomeFragment;
import xyz.jmatt.foodinventoryclient.models.ClientSingletonModel;
import xyz.jmatt.results.SimpleResult;

public class SetScanTask extends AsyncTask<String, Void, SimpleResult> {
    private BaseNavActivity activity;
    private HomeFragment fragment;

    public SetScanTask(BaseNavActivity activity, HomeFragment fragment) {
        this.activity = activity;
        this.fragment = fragment;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        activity.startLoadingModal("Setting scanner state...");
    }

    @Override
    protected SimpleResult doInBackground(String... states) {
        return ClientSingletonModel.getProxy().setScanState(states[0]);
    }

    @Override
    protected void onPostExecute(SimpleResult result) {
        super.onPostExecute(result);
        activity.dismissLoadingModal();
        if(result.isError()) {
            activity.toast(result.getMessage());
        } else {
            fragment.requestScanButtonState();
        }
    }
}
