package xyz.jmatt.foodinventoryclient.services;

import com.google.firebase.iid.FirebaseInstanceIdService;

import xyz.jmatt.foodinventoryclient.models.ClientSingletonModel;

public class FirebaseTokenService extends FirebaseInstanceIdService {
    public FirebaseTokenService() {}

    @Override
    public void onTokenRefresh() {
        ClientSingletonModel.getInstance().logCurrentToken();
    }
}
