package xyz.jmatt.foodinventoryclient.services;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import xyz.jmatt.foodinventoryclient.R;

public class FirebaseMessengerService extends FirebaseMessagingService {

    public FirebaseMessengerService() {}

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> data = remoteMessage.getData();
        if(data.containsKey("message")) {
            if(data.get("message").equals("recents")) {
                Intent intent = new Intent(getString(R.string.fcm_refresh_recents));
                LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(intent);
            } else {
                Intent intent = new Intent(getString(R.string.fcm_message));
                intent.putExtra(getString(R.string.fcm_message), data.get("message"));
                LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(intent);
            }
        }
    }
}
