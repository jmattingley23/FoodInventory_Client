package xyz.jmatt.foodinventoryclient.models;

import android.content.Context;

import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.List;

import xyz.jmatt.ServerProxy;
import xyz.jmatt.foodinventoryclient.asynctasks.UpdateTokenTask;
import xyz.jmatt.foodinventoryclient.lazyimageloader.ImageLoader;
import xyz.jmatt.models.ItemModel;

public class ClientSingletonModel {
    private static final ClientSingletonModel INSTANCE = new ClientSingletonModel();
    private static final ServerProxy proxy = new ServerProxy();
    private static ImageLoader imageLoader;

    private static List<ItemModel> recentItems = new ArrayList<>();
    private static List<ItemModel> allItems = new ArrayList<>();

    public static ClientSingletonModel getInstance() {
        return INSTANCE;
    }

    public static ServerProxy getProxy() {
        return proxy;
    }

    public void logCurrentToken() {
        String token = FirebaseInstanceId.getInstance().getToken();
        new UpdateTokenTask().execute(token);
    }

    public void setRecentItems(List<ItemModel> items) {
        recentItems = items;
    }

    public void setAllItems(List<ItemModel> items) {
        allItems = items;
    }

    public List<ItemModel> getRecentItems() {
        return recentItems;
    }

    public List<ItemModel> getAllItems() {
        return allItems;
    }

    public ItemModel getItemFromBarcode(String barcode) {
        for(ItemModel item : allItems) {
            if(item.getBarcode().equals(barcode)) {
                return item;
            }
        }
        for(ItemModel item : recentItems) {
            if(item.getBarcode().equals(barcode)) {
                return item;
            }
        }
        return null;
    }

    public void initImageLoader(Context context) {
        imageLoader = new ImageLoader(context);
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }
}
