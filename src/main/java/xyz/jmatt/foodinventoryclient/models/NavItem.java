package xyz.jmatt.foodinventoryclient.models;

public class NavItem {
    private String title;
    private int icon;
    private Class activity;

    public NavItem(String title, int icon, Class activity) {
        this.title = title;
        this.icon = icon;
        this.activity = activity;
    }

    public String getTitle() {
        return title;
    }

    public int getIcon() {
        return icon;
    }

    public Class getActivity() {
        return activity;
    }
}
