package xyz.jmatt.foodinventoryclient.lazyimageloader;

//***************
//  https://github.com/thest1/LazyList
//***************

import java.io.File;
import android.content.Context;

import xyz.jmatt.foodinventoryclient.R;

public class FileCache {

    private File cacheDir;

    public FileCache(Context context) {
        //Find the directory to save cached images
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            cacheDir = new File(android.os.Environment.getExternalStorageDirectory(), context.getString(R.string.images_directory));
        } else {
            cacheDir = context.getCacheDir();
        }
        if(!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
    }

    public File getFile(String url){
        //identify images by hashcode
        String filename=String.valueOf(url.hashCode());
        File file = new File(cacheDir, filename);
        return file;
    }

    public void clear(){
        File[] files = cacheDir.listFiles();
        if(files == null) {
            return;
        }
        for(File file : files) {
            file.delete();
        }
    }
}