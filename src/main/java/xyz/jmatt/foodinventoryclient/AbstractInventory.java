package xyz.jmatt.foodinventoryclient;

public interface AbstractInventory {
    public void updateInventory();

    public void markServerReady();
}
