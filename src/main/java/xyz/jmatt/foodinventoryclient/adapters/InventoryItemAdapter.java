package xyz.jmatt.foodinventoryclient.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import xyz.jmatt.foodinventoryclient.holders.InventoryItemHolder;
import xyz.jmatt.models.ItemModel;

public class InventoryItemAdapter extends RecyclerView.Adapter<InventoryItemHolder> {
    private List<ItemModel> items;
    private Context context;

    public InventoryItemAdapter(List<ItemModel> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public InventoryItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        return new InventoryItemHolder(inflater, parent);
    }

    @Override
    public void onBindViewHolder(InventoryItemHolder holder, int position) {
        holder.bind(items.get(position), context);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
