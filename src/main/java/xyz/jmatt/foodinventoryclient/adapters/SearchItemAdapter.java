package xyz.jmatt.foodinventoryclient.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import xyz.jmatt.foodinventoryclient.holders.SearchItemHolder;
import xyz.jmatt.models.ItemModel;

public class SearchItemAdapter extends RecyclerView.Adapter<SearchItemHolder> {
    private List<ItemModel> items;
    private Context context;

    public SearchItemAdapter(List<ItemModel> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public SearchItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        return new SearchItemHolder(inflater, parent);
    }

    @Override
    public void onBindViewHolder(SearchItemHolder holder, int position) {
        holder.bind(items.get(position), context);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
