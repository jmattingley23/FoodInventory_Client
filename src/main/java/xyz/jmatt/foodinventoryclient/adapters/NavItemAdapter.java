package xyz.jmatt.foodinventoryclient.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import xyz.jmatt.foodinventoryclient.R;
import xyz.jmatt.foodinventoryclient.models.NavItem;

public class NavItemAdapter extends ArrayAdapter<NavItem> {
    private Context context;
    private int layoutId;
    private NavItem[] items;

    public NavItemAdapter(Context context, int layoutId, NavItem[] items) {
        super(context, layoutId, items);
        this.context = context;
        this.layoutId = layoutId;
        this.items = items;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable final View convertView, @NonNull ViewGroup parent) {
        final NavItem item = items[position];

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.navbar_item, null);
        TextView title = (TextView)view.findViewById(R.id.navitem_title);
        ImageView icon = (ImageView)view.findViewById(R.id.navitem_icon);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, item.getActivity());
                context.startActivity(intent);
            }
        });

        title.setText(item.getTitle());
        icon.setImageResource(item.getIcon());

        return view;
    }
}
