package xyz.jmatt.foodinventoryclient.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import xyz.jmatt.foodinventoryclient.holders.RecentItemHolder;
import xyz.jmatt.models.ItemModel;

public class RecentItemAdapter extends RecyclerView.Adapter<RecentItemHolder> {
    private List<ItemModel> items;
    private Context context;

    public RecentItemAdapter(List<ItemModel> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public RecentItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        return new RecentItemHolder(inflater, parent);
    }

    @Override
    public void onBindViewHolder(RecentItemHolder holder, int position) {
        holder.bind(items.get(position), context);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
