package xyz.jmatt.foodinventoryclient.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import xyz.jmatt.foodinventoryclient.R;

public class UpdateImageFragment extends Fragment {
    private String imageUrl;

    private EditText urlField;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_update_image, container, false);

        Bundle args = getArguments();
        imageUrl = args.getString(getString(R.string.tag_url));

        urlField = (EditText)view.findViewById(R.id.update_image_field);
        urlField.setText(imageUrl);

        final Button saveBtn = (Button)view.findViewById(R.id.update_image_save_btn);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveItem();
            }
        });

        Button cancelBtn = (Button)view.findViewById(R.id.update_image_cancel_btn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDialog();
            }
        });

        return view;
    }

    public void saveItem() {
        imageUrl = urlField.getText().toString();
        FragmentManager fragmentManager = getFragmentManager();
        ItemFragment itemFragment = (ItemFragment)fragmentManager.findFragmentByTag(getString(R.string.tag_item_fragment));
        itemFragment.setImageLink(imageUrl);
        closeDialog();
    }

    public void closeDialog() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        UpdateImageFragment updateImageFragment = (UpdateImageFragment)fragmentManager.findFragmentByTag(getString(R.string.tag_update_image));
        fragmentTransaction.remove(updateImageFragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
        fragmentTransaction.commit();
    }
}
