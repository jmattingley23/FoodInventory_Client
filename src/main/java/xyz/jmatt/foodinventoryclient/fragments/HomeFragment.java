package xyz.jmatt.foodinventoryclient.fragments;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import xyz.jmatt.foodinventoryclient.R;
import xyz.jmatt.foodinventoryclient.activities.BaseNavActivity;
import xyz.jmatt.foodinventoryclient.adapters.RecentItemAdapter;
import xyz.jmatt.foodinventoryclient.asynctasks.GetRecentsTask;
import xyz.jmatt.foodinventoryclient.asynctasks.GetScanTask;
import xyz.jmatt.foodinventoryclient.asynctasks.ProcessItemTask;
import xyz.jmatt.foodinventoryclient.asynctasks.SetScanTask;
import xyz.jmatt.foodinventoryclient.models.ClientSingletonModel;

public class HomeFragment extends Fragment {
    private ImageButton menuBtn;
    private ImageButton searchBtn;
    private ImageButton submitBarcodeBtn;
    private TextView barcodeField;

    private Button scanInBtn;
    private Button scanOutBtn;
    private RelativeLayout refreshRecentsBtn;
    private Vibrator vibrator;

    private RecyclerView recentItemsView;
    private RecentItemAdapter recentItemAdapter;

    private BroadcastReceiver fcmReceiver;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        vibrator = (Vibrator)getActivity().getSystemService(Context.VIBRATOR_SERVICE);

        scanInBtn = (Button)view.findViewById(R.id.btn_scan_in);
        scanInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrator.vibrate(100);
                SetScanTask setScanTask = new SetScanTask((BaseNavActivity)getActivity(),HomeFragment.this);
                setScanTask.execute(getString(R.string.scan_in_tag));
            }
        });

        scanOutBtn = (Button)view.findViewById(R.id.btn_scan_out);
        scanOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrator.vibrate(100);
                SetScanTask setScanTask = new SetScanTask((BaseNavActivity)getActivity(), HomeFragment.this);
                setScanTask.execute(getString(R.string.scan_out_tag));
            }
        });

        submitBarcodeBtn = (ImageButton)view.findViewById(R.id.send_barcode_btn);
        submitBarcodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitManualBarcode();
            }
        });

        barcodeField = (TextView)view.findViewById(R.id.toolbar_home_barcode);

        refreshRecentsBtn = (RelativeLayout)view.findViewById(R.id.btn_refresh_recents);
        refreshRecentsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrator.vibrate(100);
                fetchNewRecentItems();
            }
        });

        ImageButton refreshIconBtn = (ImageButton)view.findViewById(R.id.btn_refresh_recents_icon);
        refreshIconBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrator.vibrate(100);
                fetchNewRecentItems();
            }
        });

        recentItemsView = (RecyclerView)view.findViewById(R.id.recent_items_list);
        recentItemsView.hasFixedSize();
        recentItemsView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recentItemAdapter = new RecentItemAdapter(ClientSingletonModel.getInstance().getRecentItems(), getActivity());
        fetchNewRecentItems();

        GetScanTask getScanTask = new GetScanTask((BaseNavActivity)getActivity(), this);
        getScanTask.execute();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        fcmReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                fetchNewRecentItems();
            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(fcmReceiver, new IntentFilter(getString(R.string.fcm_refresh_recents)));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(fcmReceiver);
    }

    public void submitManualBarcode() {
        String barcode = barcodeField.getText().toString();
        if(!barcode.equals("")) {
            new ProcessItemTask((BaseNavActivity)getActivity()).execute(barcode);
            barcodeField.setText("");
        }
    }

    public void requestScanButtonState() {
        GetScanTask getScanTask = new GetScanTask((BaseNavActivity)getActivity(), this);
        getScanTask.execute();
    }

    /**
     * Updates the styling of the scan in/out buttons to make sure it is in sync with the server
     */
    public void updateScanButtonStyling(String state) {
        if(state.equals(getString(R.string.scan_in_tag))) {
            scanInBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
            scanInBtn.setTextColor(getResources().getColor(R.color.text));
            scanOutBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.lightGray)));
            scanOutBtn.setTextColor(getResources().getColor(R.color.gray));
        } else {
            scanInBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.lightGray)));
            scanInBtn.setTextColor(getResources().getColor(R.color.gray));
            scanOutBtn.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
            scanOutBtn.setTextColor(getResources().getColor(R.color.text));
        }
    }

    public void fetchNewRecentItems() {
        GetRecentsTask getRecentsTask = new GetRecentsTask((BaseNavActivity)getActivity(), this);
        getRecentsTask.execute();
    }

    /**
     * Reloads the list of recent items
     */
    public void refreshRecentItems() {
        recentItemAdapter = new RecentItemAdapter(ClientSingletonModel.getInstance().getRecentItems(), getActivity());
        recentItemsView.setAdapter(recentItemAdapter);
    }
}
