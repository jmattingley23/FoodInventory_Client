package xyz.jmatt.foodinventoryclient.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import xyz.jmatt.foodinventoryclient.AbstractInventory;
import xyz.jmatt.foodinventoryclient.R;
import xyz.jmatt.foodinventoryclient.activities.BaseNavActivity;
import xyz.jmatt.foodinventoryclient.adapters.InventoryItemAdapter;
import xyz.jmatt.foodinventoryclient.asynctasks.GetAllItemsTask;
import xyz.jmatt.foodinventoryclient.asynctasks.SortAllItemsTask;
import xyz.jmatt.foodinventoryclient.models.ClientSingletonModel;

public class InventoryFragment extends Fragment implements AbstractInventory, AdapterView.OnItemSelectedListener {
    private RecyclerView inventoryView;

    private Spinner typeSpinner;
    private Spinner orderSpinner;

    private boolean isServerReady;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inventory, container, false);

        new GetAllItemsTask((BaseNavActivity)getActivity(), this).execute();

        isServerReady = false;

        inventoryView = (RecyclerView)view.findViewById(R.id.inventory_view);
        inventoryView.setLayoutManager(new GridLayoutManager(getActivity(), 3));

        typeSpinner = (Spinner)view.findViewById(R.id.inventory_sort_type_spinner);
        String[] sortTypes = {"Recently Updated", "Alphabetical", "Quantity", "Expiration Date"};
        ArrayAdapter<String> typeAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, sortTypes);
        typeSpinner.setAdapter(typeAdapter);
        typeSpinner.setOnItemSelectedListener(this);

        orderSpinner = (Spinner)view.findViewById(R.id.inventory_sort_order_spinner);
        String[] orderTypes = {"Descending", "Ascending"};
        ArrayAdapter<String> orderAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, orderTypes);
        orderSpinner.setAdapter(orderAdapter);
        orderSpinner.setOnItemSelectedListener(this);

        RelativeLayout refreshView = (RelativeLayout)view.findViewById(R.id.inventory_refresh_view);
        refreshView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshInventory();
            }
        });

        ImageButton refreshBtn = (ImageButton)view.findViewById(R.id.inventory_refresh_btn);
        refreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshInventory();
            }
        });

        return view;
    }

    public void refreshInventory() {
        isServerReady = false;
        new GetAllItemsTask((BaseNavActivity)getActivity(), this).execute();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(isServerReady) {
            new SortAllItemsTask(ClientSingletonModel.getInstance().getAllItems(), this)
                    .execute(typeSpinner.getSelectedItemPosition(),
                            orderSpinner.getSelectedItemPosition());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}

    @Override
    public void updateInventory() {
        InventoryItemAdapter inventoryItemAdapter = new InventoryItemAdapter(ClientSingletonModel.getInstance().getAllItems(), getActivity());
        inventoryView.setAdapter(inventoryItemAdapter);
    }

    @Override
    public void markServerReady() {
        isServerReady = true;
        onItemSelected(null, null, 0, 0); //perform initial sort
    }
}
