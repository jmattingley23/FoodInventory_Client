package xyz.jmatt.foodinventoryclient.fragments;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import xyz.jmatt.foodinventoryclient.R;
import xyz.jmatt.foodinventoryclient.activities.BaseNavActivity;
import xyz.jmatt.foodinventoryclient.asynctasks.UpdateItemTask;
import xyz.jmatt.foodinventoryclient.models.ClientSingletonModel;
import xyz.jmatt.models.ItemModel;

public class ItemFragment extends Fragment implements DatePickerDialog.OnDateSetListener {
    private EditText titleField;
    private EditText quantityField;
    private ImageView image;
    private EditText expirationField;
    private DatePickerDialog expDatePicker;

    private ItemModel originalModel;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item, container, false);

        Bundle args = getArguments();
        String barcodeStr = args.getString(getString(R.string.tag_barcode));
        originalModel = ClientSingletonModel.getInstance().getItemFromBarcode(barcodeStr);

        titleField = (EditText) view.findViewById(R.id.item_title);
        titleField.setText(originalModel.getTitle());

        quantityField = (EditText)view.findViewById(R.id.item_quantity);
        quantityField.setText(String.valueOf(originalModel.getQuantity()));

        TextView barcode = (TextView)view.findViewById(R.id.item_barcode);
        barcode.setText(originalModel.getFormattedBarcode());

        TextView lastAdded = (TextView)view.findViewById(R.id.item_last_added);
        Date lastAddedDate = new Date(originalModel.getLastAdded());
        SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a EEE, MMM d, yyyy", Locale.US);
        lastAdded.setText(dateFormat.format(lastAddedDate));

        image = (ImageView)view.findViewById(R.id.item_image);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showUpdateImageFragment();
            }
        });
        refreshImage();

        Button saveBtn = (Button)view.findViewById(R.id.item_save_btn);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ItemModel model = new ItemModel();
                model.setBarcode(originalModel.getBarcode());
                model.setTitle(titleField.getText().toString());
                model.setQuantity(Integer.parseInt(quantityField.getText().toString()));
                model.setLastAdded(System.currentTimeMillis());
                model.setImage(originalModel.getImage());
                model.setExpiration(originalModel.getExpiration());
                new UpdateItemTask((BaseNavActivity)getActivity()).execute(model);
            }
        });

        Button decBtn = (Button)view.findViewById(R.id.item_quantity_dec_btn);
        decBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decQuantity();
            }
        });

        Button incBtn = (Button)view.findViewById(R.id.item_quantity_inc_btn);
        incBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incQuantity();
            }
        });

        final Calendar calendar = Calendar.getInstance();
        expDatePicker = new DatePickerDialog(getActivity(), ItemFragment.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        expirationField = (EditText)view.findViewById(R.id.item_expiration);
        expirationField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!expDatePicker.isShowing()) {
                    expDatePicker.show();
                }
            }
        });
        displayExpirationDate();

        Button clearBtn = (Button)view.findViewById(R.id.expiration_clear_btn);
        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                originalModel.setExpiration(0);
                displayExpirationDate();
            }
        });

        return view;
    }

    public void decQuantity() {
        int currentValue = Integer.parseInt(quantityField.getText().toString());
        if(currentValue >= 1) {
            currentValue--;
        }
        quantityField.setText(String.valueOf(currentValue));
    }

    public void incQuantity() {
        int currentValue = Integer.parseInt(quantityField.getText().toString());
        currentValue++;
        quantityField.setText(String.valueOf(currentValue));
    }

    public void displayExpirationDate() {
        if(originalModel.getExpiration() == 0) {
            expirationField.setText("");
        } else {
            Date expirationDate = new Date(originalModel.getExpiration());
            SimpleDateFormat expirationFormat = new SimpleDateFormat("MMM, d, yyyy", Locale.US);
            expirationField.setText(expirationFormat.format(expirationDate));
        }
    }

    public void showUpdateImageFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        UpdateImageFragment fragment = (UpdateImageFragment)fragmentManager.findFragmentByTag(getString(R.string.tag_update_image));
        if(fragment == null) {
            fragment = new UpdateImageFragment();
            Bundle args = new Bundle();
            args.putString(getString(R.string.tag_url), originalModel.getImage());
            fragment.setArguments(args);
            fragmentTransaction.add(android.R.id.content, fragment, getString(R.string.tag_update_image));
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            fragmentTransaction.commit();
        }
    }

    public void setImageLink(String link) {
        originalModel.setImage(link);
        refreshImage();
    }

    public void refreshImage() {
        ClientSingletonModel.getInstance().getImageLoader().DisplayImage(originalModel.getImage(), image);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, view.getYear());
        calendar.set(Calendar.MONTH, view.getMonth());
        calendar.set(Calendar.DAY_OF_MONTH, view.getDayOfMonth());
        originalModel.setExpiration(calendar.getTimeInMillis());
        displayExpirationDate();
    }
}
