package xyz.jmatt.foodinventoryclient.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import xyz.jmatt.foodinventoryclient.R;
import xyz.jmatt.foodinventoryclient.activities.BaseNavActivity;
import xyz.jmatt.foodinventoryclient.activities.SearchActivity;


public class ActionBarFragment extends Fragment {
    private ImageButton menuBtn;
    private ImageButton searchBtn;
    private Toolbar toolbar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_actionbar, container, false);

        toolbar = (Toolbar)view.findViewById(R.id.actionbar);

        menuBtn = (ImageButton)view.findViewById(R.id.toolbar_home_menuBtn);
        menuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseNavActivity)getActivity()).openDrawer();
            }
        });

        searchBtn = (ImageButton)view.findViewById(R.id.toolbar_home_searchBtn);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchActivity.class);
                startActivity(intent);
            }
        });

        Bundle args = getArguments();
        if(args != null) {
            if(args.getBoolean(getString(R.string.tag_flatten))) {
                toolbar.setElevation(0f);
            }
            String title = args.getString(getString(R.string.tag_title));
            if(title != null) {
                TextView titleView = (TextView)view.findViewById(R.id.actionbar_title);
                titleView.setText(title);
            }
            if(args.getBoolean(getString(R.string.tag_hide_search))) {
                searchBtn.setVisibility(View.INVISIBLE);
            }
        }

        return view;
    }
}
