package xyz.jmatt.foodinventoryclient.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import xyz.jmatt.foodinventoryclient.AbstractInventory;
import xyz.jmatt.foodinventoryclient.R;
import xyz.jmatt.foodinventoryclient.activities.BaseNavActivity;
import xyz.jmatt.foodinventoryclient.adapters.SearchItemAdapter;
import xyz.jmatt.foodinventoryclient.asynctasks.GetAllItemsTask;
import xyz.jmatt.foodinventoryclient.models.ClientSingletonModel;
import xyz.jmatt.models.ItemModel;

public class SearchFragment extends Fragment implements AbstractInventory {
    private EditText searchField;

    private RecyclerView searchResultView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        searchField = (EditText)view.findViewById(R.id.search_box);
        searchField.setEnabled(false);

        setupTextWatcher();

        searchResultView = (RecyclerView)view.findViewById(R.id.search_view);
        searchResultView.setLayoutManager(new LinearLayoutManager(getActivity()));

        new GetAllItemsTask((BaseNavActivity)getActivity(), this).execute();

        return view;
    }

    public void setupTextWatcher() {
        searchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                search(s);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });
    }

    public void search(CharSequence searchString) {
        if(searchString.length() != 0) {
            String query = searchString.toString().toLowerCase();
            List<ItemModel> allItems = ClientSingletonModel.getInstance().getAllItems();
            Set<ItemModel> relevantItems = new LinkedHashSet<ItemModel>();
            for(ItemModel item : allItems) {
                if(item.getTitle().toLowerCase().contains(query) || item.getBarcode().toLowerCase().contains(query)) {
                    relevantItems.add(item);
                }
            }
            SearchItemAdapter searchItemAdapter = new SearchItemAdapter(new ArrayList<ItemModel>(relevantItems), getActivity());
            searchResultView.setAdapter(searchItemAdapter);
        }
    }

    @Override
    public void updateInventory() {}

    @Override
    public void markServerReady() {
        searchField.setEnabled(true);
    }
}
